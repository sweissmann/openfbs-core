
#include "BiotSavart.h"
#include "openfbs.h"
#include "FilamentAdvection.h"

using namespace openfbs;

int main(int argc, char **argv) {

	using openfbs::util::ellipse;

	size_t N=2000;
	VEC_SEQ fil;
	ellipse(fil, N, 2, 15);

	std::vector<VEC_SEQ > filaments;
	filaments.push_back(fil);

	if (N<2500) {
		size_t M=10000;
		VEC_SEQ fil_eval;
		ellipse(fil_eval, M, 1, 20);

		VEC_SEQ vel1, vel2;

		tic();
		biotSavart(vel1, fil_eval, filaments, 1.0, 0.1, true);
		toc("biotSavart");

		tic();
		biotSavartTree(vel2, fil_eval, filaments, 1.0, 0.1, 1E-5);
		toc("biotSavartTree");

		double maxDiff = 0;
		for (int i=0; i<vel1.size(); i++) {
			double ni = norm(vel2[i]-vel1[i]);
			if (ni > maxDiff) {
				maxDiff = ni;
				std::cout << "diff: " << vel1[i] <<" vs. " << vel2[i] << " abs diff: " << ni << std::endl;
			}
		}
		std::cout << "max diff: "<<maxDiff<<std::endl;
	}

	double strength(1.0);
	double a(0.1);

	tic();
	advect::dopri5(filaments, strength, a, 0.1, 10);
	toc("dopri5 10x0.1");

	tic();
	advect::rk4(filaments, strength, a, 0.1, 10);
	toc("rk4 10x0.1");

	tic();
	advect::euler(filaments, strength, a, 0.1, 10);
	toc("euler 10x0.1");

	VEC_SEQ vel;

	tic();
	biotSavartTree(vel, fil, filaments, 1.0, 0.1, 1E-6);
	toc("biotSavartTree");

	tic();
	biotSavartTree(vel, fil, filaments, 1.0, 0.1, 1E-6);
	toc("biotSavartTree");

}
