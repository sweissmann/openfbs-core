#include "FilamentSystem.h"
#include "BiotSavart.h"
#include "openfbs.h"
#include "FilamentReconnection.h"

#include <iostream>

using namespace openfbs;

static void CPPUNIT_ASSERT_EQUAL(size_t expected, size_t result) {
	if (expected != result) {
		std::cout << "ASSERTION FAILED: was " << result << " but expected " << expected << std::endl;
	} else {
		std::cout << "ASSERTION OK: " << result << std::endl;
	}
}

int main(int argc, char **argv) {

	FilamentSystem* connected = new FilamentSystem();
	FilamentSystem* disjoint = new FilamentSystem();

	VEC_SEQ* pts_left = new VEC_SEQ;
	pts_left->push_back(Vec(-2,0,0));
	pts_left->push_back(Vec(-1,0,0));
	pts_left->push_back(Vec(-1,0,1));
	pts_left->push_back(Vec(-2,0,1));

	VEC_SEQ* pts_right = new VEC_SEQ;
	pts_right->push_back(Vec(2,0,0));
	pts_right->push_back(Vec(2,0,1));
	pts_right->push_back(Vec(1,0,1));
	pts_right->push_back(Vec(1,0,0));

	VEC_SEQ* pts_join = new VEC_SEQ;
	pts_join->push_back(Vec(-2,0,0));
	pts_join->push_back(Vec(-1,0,0));
	pts_join->push_back(Vec(1,0,0));
	pts_join->push_back(Vec(2,0,0));
	pts_join->push_back(Vec(2,0,1));
	pts_join->push_back(Vec(1,0,1));
	pts_join->push_back(Vec(-1,0,1));
	pts_join->push_back(Vec(-2,0,1));

	disjoint->addFilament(*pts_left);
	disjoint->addFilament(*pts_right);
	connected->addFilament(*pts_join);

	// testJoin:
	{
		FilamentTopology topo(*disjoint);
		std::vector<VEC_SEQ > orig;
		topo.convert_to_filaments(orig, 0);

		CPPUNIT_ASSERT_EQUAL(size_t(2), orig.size());
		CPPUNIT_ASSERT_EQUAL(size_t(4), orig[0].size());
		CPPUNIT_ASSERT_EQUAL(size_t(4), orig[1].size());

		topo.do_reconnect(1,6);
		std::vector<VEC_SEQ > recon;
		topo.convert_to_filaments(recon, 0);

		CPPUNIT_ASSERT_EQUAL(size_t(1), recon.size());
		CPPUNIT_ASSERT_EQUAL(size_t(8), recon[0].size());
	}
	// testSplit:
	{
		FilamentTopology topo(*connected);
		std::vector<VEC_SEQ > orig;
		topo.convert_to_filaments(orig, 0);
		CPPUNIT_ASSERT_EQUAL(size_t(1), orig.size());
		CPPUNIT_ASSERT_EQUAL(size_t(8), orig[0].size());


		topo.do_reconnect(1, 5);
		std::vector<VEC_SEQ > recon;
		topo.convert_to_filaments(recon, 0);

		CPPUNIT_ASSERT_EQUAL(size_t(2), recon.size());
		CPPUNIT_ASSERT_EQUAL(size_t(4), recon[0].size());
		CPPUNIT_ASSERT_EQUAL(size_t(4), recon[1].size());
	}

	// testTwice:
	{
		FilamentTopology topo(*connected);
		std::vector<VEC_SEQ > orig;
		topo.convert_to_filaments(orig, 0);
		CPPUNIT_ASSERT_EQUAL(size_t(1), orig.size());
		CPPUNIT_ASSERT_EQUAL(size_t(8), orig[0].size());


		topo.do_reconnect(1, 6);
		topo.do_reconnect(1, 6);

		std::vector<VEC_SEQ > recon;
		topo.convert_to_filaments(recon, 0);
		CPPUNIT_ASSERT_EQUAL(size_t(1), recon.size());
		CPPUNIT_ASSERT_EQUAL(size_t(8), recon[0].size());
	}

	// testStress:
	{
//	srand(time(NULL));

//	clock_t start = clock();

	size_t LEN = 1024*10;
	VEC_SEQ long_poly;
	openfbs::util::ellipse(long_poly, LEN, 100, 200);

	FilamentSystem sys;
	sys.addFilament(long_poly);

	FilamentTopology topo(sys);

	for (size_t i=0; i<LEN; ++i) {
		size_t i1 = rand()%LEN;
		size_t i2 = rand()%LEN;
		topo.do_reconnect(i1, i2);
	}
	std::vector<VEC_SEQ > res;
	topo.convert_to_filaments(res, 0);

//	clock_t end = clock();
//	double cpu_time = static_cast<double>( end - start ) / CLOCKS_PER_SEC;

//	std::cout << "extracted "<<res.size()<<" filaments after reconnection. cpu_time="<< cpu_time << std::endl;
	size_t total=0;
	for (std::vector<VEC_SEQ >::const_iterator ci = res.begin(); ci != res.end(); ++ci) {
//		std::cout << "\tlen="<<ci->numPoints()<<std::endl;
		total += ci->size();
	}
//	std::cout << "total points: "<<total<<" of "<<LEN<<", lost "<<(LEN-total)<<" points."<<std::endl;

	CPPUNIT_ASSERT_EQUAL(LEN, total);
	}
	// testNoop() {
	{
		FilamentTopology topo(*disjoint);
		std::vector<VEC_SEQ > orig;
		topo.convert_to_filaments(orig, 0);
		CPPUNIT_ASSERT_EQUAL(size_t(2), orig.size());
	}

}


//void ReconnectionTest::testStressGrid() {
//
////	srand(time(NULL));
//
//	tic();
//
//	size_t LEN = 1024;
//	VEC_SEQ long_poly;
//	ellipse(long_poly, LEN, 100, 200);
//
//	Filament fil(long_poly);
//
//	double avEL = fil.length()/LEN;
//
//	FilamentSystem sys;
//	sys.addFilament(fil);
//
//	sys.reconnect(avEL, 0.75, 0);
////	toc_msg() << "extracted "<<sys.numFilaments()<<" filaments after reconnection." << std::endl;
////	size_t total=0;
////	for (std::vector<Filament>::const_iterator ci = res.begin(); ci != res.end(); ++ci) {
////		std::cout << "\tlen="<<ci->numPoints()<<std::endl;
////		total += ci->numPoints();
////	}
////	std::cout << "total points: "<<total<<" of "<<LEN<<", lost "<<(LEN-total)<<" points."<<std::endl;
//}
