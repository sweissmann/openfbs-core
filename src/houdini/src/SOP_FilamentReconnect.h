/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#ifndef __SOP_FilamentReconnect_h__
#define __SOP_FilamentReconnect_h__

#include <SOP/SOP_Node.h>

namespace openfbs {

class SOP_FilamentReconnect : public SOP_Node
{
public:
	SOP_FilamentReconnect(OP_Network *net, const char *name, OP_Operator *op);
    virtual ~SOP_FilamentReconnect();

    static PRM_Template    myTemplateList[];
    static OP_Node        *myConstructor(OP_Network*, const char *,
                                OP_Operator *);

protected:
    virtual const char      *inputLabel(unsigned idx) const;

    /// Method to cook geometry for the SOP
    virtual OP_ERROR         cookMySop(OP_Context &context);

private:
    fpreal  A(fpreal t) { return evalFloat("a", 0, t); }
    fpreal  LAMBDA(fpreal t) { return evalFloat("lambda", 0, t); }
    fpreal  HAIRPIN_ANGLE(fpreal t) { return evalFloat("hairpin_angle", 0, t); }
    fpreal  SEARCH_SIZE(fpreal t) { return evalFloat("search_size", 0, t); }
    int  MIN_SIZE(fpreal t)         { return evalInt("min_size", 0, t); }
    int  ITERATIONS(fpreal t)         { return evalInt("iterations", 0, t); }
};
} // End openfbs namespace

#endif
