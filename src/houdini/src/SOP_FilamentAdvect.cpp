/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#include "SOP_FilamentAdvect.h"

#include <GU/GU_PrimPoly.h>

#include <PRM/PRM_Default.h>
#include <PRM/PRM_Range.h>
#include <PRM/PRM_ChoiceList.h>

#include "FilamentAdvection.h"
#include "Darboux.h"

using namespace openfbs;

enum SOP_AvectionMethod
{
    ADVECTON_EULER,
    ADVECTION_RK4,
    ADVECTION_DOPRI5
};

static PRM_Name avectionMenuNames[] =
{
    PRM_Name("euler", "Euler"),
    PRM_Name("rk4", "RK4"),
    PRM_Name("dopri5", "DOPRI5"),
    PRM_Name(0)
};
static PRM_ChoiceList advectionMenu(PRM_CHOICELIST_SINGLE, avectionMenuNames);
static PRM_Default advectionDefault(ADVECTON_EULER);

static PRM_Name        names[] = {
	    PRM_Name("strength",    "Strength"),
	    PRM_Name("a",    "Filament thickness"),
	    PRM_Name("dt",    "Time step"),
	    PRM_Name("substeps",    "Substeps"),
	    PRM_Name("advection", "Advection"),
	    PRM_Name("darboux_bs_mix", "Biot-Savart vs. Darboux"),
		PRM_Name("max_vertex_count", "MAX vertices"),
};

static PRM_Range mix_range(PRM_RANGE_RESTRICTED, -1, PRM_RANGE_RESTRICTED, 1);
static PRM_Default maxVertexDefault(5000);


PRM_Template
SOP_FilamentAdvect::myTemplateList[] = {
	    PRM_Template(PRM_FLT_J, 1, &names[0], PRMpointFiveDefaults),
	    PRM_Template(PRM_FLT_J, 1, &names[1], PRMpointOneDefaults),
	    PRM_Template(PRM_FLT_J, 1, &names[2], PRMpointOneDefaults),
	    PRM_Template(PRM_INT_J, 1, &names[3], PRMoneDefaults),
	    PRM_Template(PRM_ORD,   1, &names[4], &advectionDefault, &advectionMenu),
	    PRM_Template(PRM_FLT_J, 1, &names[5], PRMzeroDefaults, 0, &mix_range),
	    PRM_Template(PRM_INT_J, 1, &names[6], &maxVertexDefault),
	    PRM_Template(),
};


OP_Node *
SOP_FilamentAdvect::myConstructor(OP_Network *net, const char *name,
OP_Operator *op)
{
    return new SOP_FilamentAdvect(net, name, op);
}

SOP_FilamentAdvect::SOP_FilamentAdvect(OP_Network *net, const char *name, OP_Operator *op)
    : SOP_Node(net, name, op)
{
}

SOP_FilamentAdvect::~SOP_FilamentAdvect()
{
}

OP_ERROR
SOP_FilamentAdvect::cookMySop(OP_Context &context)
{
    fpreal t = context.getTime();

    fpreal strength, a, dt, darboux_bs_mix;

    int substeps, advectionScheme, max_vertex_count;

    // Before we do anything, we must lock our inputs.  Before returning,
    //    we have to make sure that the inputs get unlocked.
    if (lockInputs(context) >= UT_ERROR_ABORT)
	return error();

    strength = STRENGTH(t);
    a = A(t);
    dt = DT(t);
    substeps = SUBSTEPS(t);
    advectionScheme = ADVECTION_SCHEME(t);
    darboux_bs_mix = DARBOUX_BS_MIX(t);

    max_vertex_count = MAX_VERTEX_COUNT(t);

	double bs_scale = fmin(1.0, 1.0-darboux_bs_mix);

    gdp->stashAll();

    // Get first input
    const GU_Detail *incoming_filaments = inputGeo(0);

	// read all polygons of incoming geometry:
	std::vector<VEC_SEQ > filaments;

	int n_vertices = 0;

	int N_IN = incoming_filaments->primitives().entries();
	for (int i=0; i<N_IN; ++i) {
		const GEO_Primitive * prim = incoming_filaments->primitives()(i);
		if (prim->getTypeId() == GEO_PRIMPOLY) {
			GEO_PrimPoly * poly = (GEO_PrimPoly *) prim;
			if (poly->isClosed()) {
				int n_pts = poly->getVertexCount();
				n_vertices += n_pts;
				if (n_vertices > max_vertex_count) {
					return error();
				}
				VEC_SEQ fil;
				for (int j=0; j<n_pts; ++j) {
					UT_Vector3 v = poly->getVertexElement(j).getPos3();
					fil.push_back(Vec3d(v[0], v[1], v[2]));
				}
				filaments.push_back( fil );
			} else {
				// WARN about ignored open filament...
			}
		}
	}

    for (int step = 0; step < substeps; ++step) {
		//okay, now let's simulate a time step:
		if (darboux_bs_mix < 1.0) {
			tic();
			switch (advectionScheme) {
			case ADVECTON_EULER:
				openfbs::advect::euler(filaments, strength, a, bs_scale*dt, 1);
				break;
			case ADVECTION_RK4:
				openfbs::advect::rk4(filaments, strength, a, bs_scale*dt, 1);
				break;
			case ADVECTION_DOPRI5:
				openfbs::advect::dopri5(filaments, strength, a, bs_scale*dt, 1);
				break;
			default:
				std::cout << "Unknown advection!!!" << std::endl;
			}
			toc("advect_impl ");
		}

		if (darboux_bs_mix > -1.0) {
			openfbs::darboux::time_evolution(filaments, dt, strength, a, darboux_bs_mix);
		}
    }

	for (std::vector<VEC_SEQ>::const_iterator ci = filaments.begin(); ci != filaments.end(); ci++) {
		int N = ci->size();
		GU_PrimPoly *poly = GU_PrimPoly::build(gdp, N, GU_POLY_CLOSED);
		for (int i=0; i<N; i++) {
			gdp->setPos3(poly->getPointOffset(i), (*ci)[i][0], (*ci)[i][1], (*ci)[i][2]);
		}
	}

	gdp->destroyStashed();

    unlockInputs();

    return error();
}

const char *
SOP_FilamentAdvect::inputLabel(unsigned) const
{
    return "Filaments";
}
