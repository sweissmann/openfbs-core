/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#ifndef __SOP_FilamentSubdivide_h__
#define __SOP_FilamentSubdivide_h__

#include <SOP/SOP_Node.h>

namespace openfbs {

class SOP_FilamentSubdivide : public SOP_Node
{
public:
	SOP_FilamentSubdivide(OP_Network *net, const char *name, OP_Operator *op);
    virtual ~SOP_FilamentSubdivide();

    static PRM_Template    myTemplateList[];
    static OP_Node        *myConstructor(OP_Network*, const char *,
                                OP_Operator *);

protected:
    virtual const char      *inputLabel(unsigned idx) const;

    /// Method to cook geometry for the SOP
    virtual OP_ERROR         cookMySop(OP_Context &context);

private:

    int  SUBDIV_METHOD(fpreal t)         { return evalInt("subdiv_method", 0, t); }
    fpreal  MAX_EDGE_LENGTH(fpreal t)         { return evalFloat("max_edge_len", 0, t); }
    fpreal  MAX_CURVATURE_LENGTH(fpreal t)         { return evalFloat("max_curvature_len", 0, t); }

};
} // End openfbs namespace

#endif
