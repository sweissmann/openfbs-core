/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#include "SOP_FilamentAdvect.h"
#include "SOP_FilamentReconnect.h"
#include "SOP_FilamentSubdivide.h"

#include <UT/UT_DSOVersion.h>
#include <OP/OP_Director.h>

using namespace openfbs;

void
newSopOperator(OP_OperatorTable *table)
{

    table->addOperator(new OP_Operator("openfbs_filament_advect",
                   "OPENFBS Filament Advect",
                   SOP_FilamentAdvect::myConstructor,
				   SOP_FilamentAdvect::myTemplateList,
                    1,
                    1,
                    0));

    table->addOperator(new OP_Operator("openfbs_filament_reconnect",
                   "OPENFBS Filament Reconnect",
                   SOP_FilamentReconnect::myConstructor,
				   SOP_FilamentReconnect::myTemplateList,
                    1,
                    1,
                    0));


    table->addOperator(new OP_Operator("openfbs_filament_subdivide",
                   "OPENFBS Filament Subdivide",
                   SOP_FilamentSubdivide::myConstructor,
				   SOP_FilamentSubdivide::myTemplateList,
                    1,
                    1,
                    0));

}


