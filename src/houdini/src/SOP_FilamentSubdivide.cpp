/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#include "SOP_FilamentSubdivide.h"

#include <GU/GU_PrimPoly.h>
#include <PRM/PRM_Default.h>
#include <PRM/PRM_Range.h>
#include <PRM/PRM_ChoiceList.h>

#include "Subdivider.h"

using namespace openfbs;

static PRM_Name subdivisionMenuNames[] =
{
    PRM_Name("linear", "Linear"),
    PRM_Name("cubic", "Cubic"),
    PRM_Name(0)
};

static PRM_ChoiceList subdivisionMenu(PRM_CHOICELIST_SINGLE, subdivisionMenuNames);
static PRM_Default subdivisionDefault(subdivide::LINEAR);

static PRM_Name        names[] = {
		PRM_Name("subdiv_method", "Method"),
	    PRM_Name("max_edge_len",    "Max edge length"),
	    PRM_Name("max_curvature_len",    "Max curvature length"),
};

PRM_Template
SOP_FilamentSubdivide::myTemplateList[] = {
	    PRM_Template(PRM_ORD,   1, &names[0], &subdivisionDefault, &subdivisionMenu),
	    PRM_Template(PRM_FLT_J, 1, &names[1], PRMpointFiveDefaults),
	    PRM_Template(PRM_FLT_J, 1, &names[2], PRMpointOneDefaults),
	    PRM_Template(),
};


OP_Node *
SOP_FilamentSubdivide::myConstructor(OP_Network *net, const char *name,
OP_Operator *op)
{
    return new SOP_FilamentSubdivide(net, name, op);
}

SOP_FilamentSubdivide::SOP_FilamentSubdivide(OP_Network *net, const char *name, OP_Operator *op)
    : SOP_Node(net, name, op)
{
}

SOP_FilamentSubdivide::~SOP_FilamentSubdivide()
{
}

OP_ERROR
SOP_FilamentSubdivide::cookMySop(OP_Context &context)
{
    fpreal t = context.getTime();

    fpreal maxEdgeLen, maxCurvatureLen;

    subdivide::SUBDIVISION_METHOD subdiv_method;

    // Before we do anything, we must lock our inputs.  Before returning,
    //    we have to make sure that the inputs get unlocked.
    if (lockInputs(context) >= UT_ERROR_ABORT)
	return error();

    maxEdgeLen = MAX_EDGE_LENGTH(t);
    maxCurvatureLen = MAX_CURVATURE_LENGTH(t);
	subdiv_method = subdivide::SUBDIVISION_METHOD( SUBDIV_METHOD(t) );

    gdp->stashAll();

    // Get first input
    const GU_Detail *incoming_filaments = inputGeo(0);

	// read all polygons of incoming geometry:
	std::vector<VEC_SEQ > filaments;

	int N_IN = incoming_filaments->primitives().entries();
	for (int i=0; i<N_IN; ++i) {
		const GEO_Primitive * prim = incoming_filaments->primitives()(i);
		if (prim->getTypeId() == GEO_PRIMPOLY) {
			GEO_PrimPoly * poly = (GEO_PrimPoly *) prim;
			if (poly->isClosed()) {
				int n_pts = poly->getVertexCount();
				VEC_SEQ fil;
				for (int j=0; j<n_pts; ++j) {
					UT_Vector3 v = poly->getVertexElement(j).getPos3();
					fil.push_back(Vec3d(v[0], v[1], v[2]));
				}
				filaments.push_back( fil );
			} else {
				// WARN about ignored open filament...
			}
		}
	}

	std::vector<VEC_SEQ > subdiv_filaments(filaments);

	subdivide::all(subdiv_filaments, filaments, subdiv_method, true, maxEdgeLen, maxCurvatureLen);

	for (std::vector<VEC_SEQ>::const_iterator ci = subdiv_filaments.begin(); ci != subdiv_filaments.end(); ci++) {
		int N = ci->size();
		GU_PrimPoly *poly = GU_PrimPoly::build(gdp, N, GU_POLY_CLOSED);
		for (int i=0; i<N; i++) {
			gdp->setPos3(poly->getPointOffset(i), (*ci)[i][0], (*ci)[i][1], (*ci)[i][2]);
		}
	}

	gdp->destroyStashed();

    unlockInputs();

    return error();
}

const char *
SOP_FilamentSubdivide::inputLabel(unsigned) const
{
    return "Filaments";
}
