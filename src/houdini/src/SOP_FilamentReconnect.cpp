/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#include "SOP_FilamentReconnect.h"

#include <GU/GU_PrimPoly.h>
#include <PRM/PRM_Default.h>

#include "FilamentReconnection.h"

using namespace openfbs;

static PRM_Name        names[] = {
	    PRM_Name("a",    "a (Rosenhead-Moore)"),
	    PRM_Name("lambda",    "Reconnection lambda"),
	    PRM_Name("hairpin_angle",    "Hairpin angle"),
	    PRM_Name("search_size",    "Search radius"),
		PRM_Name("min_size",    "Min edge count"),
		PRM_Name("iterations",    "Iterations"),
};


PRM_Template
SOP_FilamentReconnect::myTemplateList[] = {
	    PRM_Template(PRM_FLT_J, 1, &names[0], PRMpointOneDefaults ),
	    PRM_Template(PRM_FLT_J, 1, &names[1], PRMoneDefaults ),
	    PRM_Template(PRM_FLT_J, 1, &names[2], PRMpointFiveDefaults ),
	    PRM_Template(PRM_FLT_J, 1, &names[3], PRMpointOneDefaults ),
		PRM_Template(PRM_INT_J, 1, &names[4], PRMfourDefaults ),
		PRM_Template(PRM_INT_J, 1, &names[5], PRMoneDefaults),
	    PRM_Template(),
};


OP_Node *
SOP_FilamentReconnect::myConstructor(OP_Network *net, const char *name,
OP_Operator *op)
{
    return new SOP_FilamentReconnect(net, name, op);
}

SOP_FilamentReconnect::SOP_FilamentReconnect(OP_Network *net, const char *name, OP_Operator *op)
    : SOP_Node(net, name, op)
{
}

SOP_FilamentReconnect::~SOP_FilamentReconnect()
{
}

OP_ERROR
SOP_FilamentReconnect::cookMySop(OP_Context &context)
{
    fpreal t = context.getTime();

    fpreal a, search_radius, lambda, hairpin_angle;

    int iterations, min_size;

    // Before we do anything, we must lock our inputs.  Before returning,
    //    we have to make sure that the inputs get unlocked.
    if (lockInputs(context) >= UT_ERROR_ABORT)
	return error();

    a = A(t);
    lambda = LAMBDA(t);
    hairpin_angle = HAIRPIN_ANGLE(t);
    search_radius = SEARCH_SIZE(t);
    min_size = MIN_SIZE(t);
    iterations = ITERATIONS(t);

    gdp->stashAll();

    // Get first input
    const GU_Detail *incoming_filaments = inputGeo(0);

	// read all polygons of incoming geometry:
	std::vector<VEC_SEQ > filaments;

	int N_IN = incoming_filaments->primitives().entries();
	for (int i=0; i<N_IN; ++i) {
		const GEO_Primitive * prim = incoming_filaments->primitives()(i);
		if (prim->getTypeId() == GEO_PRIMPOLY) {
			GEO_PrimPoly * poly = (GEO_PrimPoly *) prim;
			if (poly->isClosed()) {
				int n_pts = poly->getVertexCount();
				VEC_SEQ fil;
				for (int j=0; j<n_pts; ++j) {
					UT_Vector3 v = poly->getVertexElement(j).getPos3();
					fil.push_back(Vec3d(v[0], v[1], v[2]));
				}
				filaments.push_back( fil );
			} else {
				// WARN about ignored open filament...
			}
		}
	}

	for (int i=0; i<iterations; i++) {
		reconnection::reconnect( filaments, a, search_radius, lambda, hairpin_angle, min_size );
	}

	for (std::vector<VEC_SEQ >::const_iterator ci = filaments.begin(); ci != filaments.end(); ci++) {
		int N = ci->size();
		GU_PrimPoly *poly = GU_PrimPoly::build(gdp, N, GU_POLY_CLOSED);
		for (int i=0; i<N; i++) {
			gdp->setPos3(poly->getPointOffset(i), (*ci)[i][0], (*ci)[i][1], (*ci)[i][2]);
		}
	}

	gdp->destroyStashed();

    unlockInputs();

    return error();
}

const char *
SOP_FilamentReconnect::inputLabel(unsigned) const
{
    return "Filaments";
}
