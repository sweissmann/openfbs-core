/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#include <boost/numeric/odeint.hpp>
#include "FilamentAdvection.h"

#include "treecode.h"

namespace openfbs {

FilamentODE::FilamentODE() :
n_pts(0), strength(0.0), a(0.0), tol(1E-9), n_leaf(256), fil_lengths(0)
{}

void FilamentODE::prepareInstance(
		//const FilamentSystem& sys
		const std::vector<VEC_SEQ > & _fils,
		double _strength,
		double _a
	) {
	FilamentODE& ode = FilamentODE::getInstance();

	ode.strength = _strength;
	ode.a = _a;
	ode.tol = 1E-9;
	ode.n_leaf = 256;

	ode.n_pts = 0;
	ode.fil_lengths.resize(_fils.size());
	for (size_t i=0; i<_fils.size(); ++i) {
		int ni = _fils[i].size();
		ode.fil_lengths[i] = ni;
		ode.n_pts += ni;
	}
}

FilamentODE& FilamentODE::getInstance() {
	static FilamentODE ode;
	return ode;
}

void FilamentODE::eval(const filament_state_type & X, filament_state_type & DX_DT, double t) {
	FilamentODE::getInstance().evaluate(X, DX_DT, t);
}

void FilamentODE::evaluate(const filament_state_type & X, filament_state_type & DX_DT, double t) {
	treecode::compute_velocity(&DX_DT[0], &X[0], n_pts,
			&X[0], &fil_lengths[0], fil_lengths.size(),
			strength, a, tol, n_leaf);
}

namespace advect {

static void read_state(const std::vector<VEC_SEQ > & filaments, filament_state_type & X) {
	// TODO: save space:
	VEC_SEQ pts;
	util::read_point_positions( pts, filaments );
	X.clear();
	for (VEC_SEQ::const_iterator ci = pts.begin(); ci != pts.end(); ++ci) {
		X.push_back((*ci)[0]);
		X.push_back((*ci)[1]);
		X.push_back((*ci)[2]);
	}
}

static void write_state(std::vector<VEC_SEQ > & filaments, const filament_state_type & X) {
	// TODO: save space:
	VEC_SEQ pts;
	for (filament_state_type::const_iterator ci=X.begin(); ci!=X.end(); ) {
		double x = (*ci);
		++ci;
		double y = (*ci);
		++ci;
		double z = (*ci);
		++ci;
		pts.push_back(Vec3d( x, y, z ));
	}
	util::write_point_positions( pts, filaments );
}

void dopri5(std::vector<VEC_SEQ > & filaments, double strength, double a, double dt, int substeps) {
	FilamentODE::prepareInstance(filaments, strength, a);
	filament_state_type pts;
	read_state(filaments, pts);
	typedef boost::numeric::odeint::runge_kutta_dopri5< filament_state_type > stepper_type;
	boost::numeric::odeint::integrate_const( stepper_type(), FilamentODE::eval, pts , 0.0 , dt , dt/substeps );
	write_state(filaments, pts);
}

void euler(std::vector<VEC_SEQ > & filaments, double strength, double a, double dt, int substeps) {
	FilamentODE::prepareInstance(filaments, strength, a);
	filament_state_type pts;
	read_state(filaments, pts);
	typedef boost::numeric::odeint::euler< filament_state_type > stepper_type;
	boost::numeric::odeint::integrate_const( stepper_type(), FilamentODE::eval , pts , 0.0 , dt , dt/substeps );
	write_state(filaments, pts);
}

void rk4(std::vector<VEC_SEQ > & filaments, double strength, double a, double dt, int substeps) {
	FilamentODE::prepareInstance(filaments, strength, a);
	filament_state_type pts;
	read_state(filaments, pts);
//	typedef boost::numeric::odeint::runge_kutta4< filament_state_type > stepper_type;
	typedef boost::numeric::odeint::runge_kutta4_classic< filament_state_type > stepper_type;
	boost::numeric::odeint::integrate_const( stepper_type(), FilamentODE::eval , pts , 0.0 , dt , dt/substeps );
	write_state(filaments, pts);
}

}

}


