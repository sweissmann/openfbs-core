/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#ifndef OPENFBS_H_
#define OPENFBS_H_

#define M_USE_MATH_DEFINES
#include <cmath>

#include <fstream>
#include <ostream>
#include <sstream>
#include <vector>

namespace openfbs {

class Vec3d;

typedef std::vector< Vec3d > VEC_SEQ;
typedef std::pair< size_t, size_t > EDGE;
typedef std::vector< EDGE > EDGE_SEQ;
typedef std::pair< size_t, size_t > PREV_NEXT_IDX;
typedef std::pair<size_t, PREV_NEXT_IDX > PREV_NEXT;
typedef std::vector< PREV_NEXT > PREV_NEXT_SEQ;

template <typename TA, typename TB> std::ostream& operator<<(std::ostream& os, const std::pair<TA, TB > & ei) {
	os << "("<<ei.first<<","<<ei.second<<")";
	return os;
}

template <typename T> std::ostream& operator<<(std::ostream& os, const std::vector<T > & seq) {
	os << "(";
	for (typename std::vector<T >::const_iterator it = seq.begin(); it != seq.end(); ++it) {
		os << (*it);
		os << ( (it+1) != seq.end() ? ", "  : ")");
	}
	return os;
}

}

void tic();
void toc(const char* msg="");
std::ostream & toc_msg();

#endif /* OPENFBS_H_ */
