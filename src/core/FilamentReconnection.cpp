/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#include "FilamentReconnection.h"
#include "BiotSavart.h"

#include <boost/foreach.hpp>

using namespace openfbs;

FilamentTopology::FilamentTopology(const std::vector<VEC_SEQ > & fils, double _a ): edge_labels(), pts(), idx_ptr(), idx(), a(_a) {
	util::read_point_positions(pts, fils);
	std::vector<int> fil_lengths;
	util::generate_filament_lengths(fil_lengths, fils);
	util::generate_edge_pointers(idx_ptr, fil_lengths);
	util::generate_edge_indices(idx, fil_lengths);
	edge_labels.resize(idx_ptr.size(), false);
}

bool FilamentTopology::is_reconnect(const size_t i1, const size_t i2, const double lambda, const double hairpin_angle) {

	if (i1==i2) return false;

	if (edge_labels[i1] || edge_labels[i2]) return false;

	EDGE e1 = idx[i1];
	EDGE e2 = idx[i2];

	Vec3d e11 = pts[e1.first];
	Vec3d e12 = pts[e1.second];
	Vec3d e21 = pts[e2.first];
	Vec3d e22 = pts[e2.second];

	if (e1.first == e2.second || e1.second == e2.first) {
		Vec3d en1 = e12-e11;
		Vec3d en2 = e22-e21;
		normalize(en1);
		normalize(en2);
		double cos_angle = dot(en1, en2);
		if (cos_angle < hairpin_angle) {
			std::cout << "removing hairpin!" << std::endl;
			do_reconnect(i1, i2);
			return true;
		}
		return false;
	}

	double dL;
	delta_length(dL, e11, e12, e22, e21);

	if (dL >= 0) return false;

//	std::cout << "checking reconnection: ("<<i1<<","<<i2<<"): "<<dL<<" ::: "<<e1<<","<<e2<<std::endl;

	double dE;
	quad_energy(dE, e11, e12, e22, e21, a);

	double final = lambda*dL + dE;

	if (final < 0) {

//		std::cout << "reconnecting "<<e1<<" and "<<e2<< "because dE="<<dE<<" final="<<final <<std::endl;

		do_reconnect(i1, i2);
		return true;
	} else {
//		std::cout << " ****** no reconnection: dL=" << dL << " dE="<< dE << std::endl;
	}

	return false;
}

void FilamentTopology::do_reconnect(const size_t i1, const size_t i2) {
	PREV_NEXT & e1 = idx_ptr[i1];
	PREV_NEXT & e2 = idx_ptr[i2];
//	size_t e_e1_p = e1.second.first;
//	size_t e_e2_p = e2.second.first;
	size_t e_e1_n = e1.second.second;
	size_t e_e2_n = e2.second.second;

	e1.second.second = e_e2_n;
	idx_ptr[e_e2_n].second.first = i1;

	e2.second.second = e_e1_n;
	idx_ptr[e_e1_n].second.first = i2;

	edge_labels[i1] = true;
	edge_labels[i2] = true;
	edge_labels[e1.second.first] = true;
	edge_labels[e1.second.second] = true;
	edge_labels[e2.second.first] = true;
	edge_labels[e2.second.second] = true;
}

void FilamentTopology::convert_to_filaments(std::vector<VEC_SEQ > & fils, const size_t min_edges) {
//	std::cout << "Converting back to filaments..."<<std::endl;
	std::vector<size_t> poly;
	std::vector< std::vector<size_t> > polys;

	size_t N_E = idx_ptr.size();
	std::vector<bool> labels(N_E, true);
	int ce=0;
	for (size_t i = 0; i<idx_ptr.size(); i++) {
		PREV_NEXT e = idx_ptr[ce];
		labels[ce]=false;
		poly.push_back(e.first);

//		std::cout<<"check: "<<poly[0]<<" == "<<e.second.second<<std::endl;

		if (poly[0] == e.second.second) {
//			std::cout << "\tfound poly with "<<poly.size()<<" points."<<std::endl;
			polys.push_back(poly);
			poly.clear();
			ce=0;
			for (std::vector<bool>::const_iterator bit = labels.begin(); bit != labels.end(); ++bit) {
				if (*bit) break;
				++ce;
			}
		} else {
			ce = e.second.second;
		}
	}
	fils.clear();
	for (std::vector< std::vector<size_t> >::const_iterator pi = polys.begin(); pi!=polys.end(); ++pi) {
		if (pi->size() >= min_edges) {
			VEC_SEQ fil_pts;
			for (std::vector<size_t>::const_iterator pt_i = pi->begin(); pt_i != pi->end(); ++pt_i) {
				fil_pts.push_back(pts[*pt_i]);
			}
			fils.push_back(fil_pts);
		}
	}
//	std::cout << "Returning "<<fils.size()<<" polygons. min_edges="<<min_edges<<std::endl;
}

ReconnectionTree::ReconnectionTree(const std::vector<VEC_SEQ > & fils, double _a, double _search_size, double _lambda, double _hairpin_angle):
		rtree(), search_size(_search_size), lambda(_lambda), hairpin_angle(_hairpin_angle), topo(fils, _a)
		{}

void ReconnectionTree::edges_to_tree() {
	size_t N = topo.numEdges();
	Vec3d ec;
	for (size_t i=0; i<N; ++i) {
		topo.edge_center(ec, i);
		point p(ec[0], ec[1], ec[2]);
		rtree.insert(value(p, i));
	}
}

void ReconnectionTree::reconnect_tree() {
	size_t N = topo.numEdges();
	Vec3d ec;
	for (size_t i=0; i<N; i+=2) {
		topo.edge_center(ec, i);
		point p1(ec[0]-search_size, ec[1]-search_size, ec[2]-search_size);
		point p2(ec[0]+search_size, ec[1]+search_size, ec[2]+search_size);
		box b(p1, p2);
		std::vector<value> res;
		rtree.query(bgi::within(b), std::back_inserter(res));
		BOOST_FOREACH(value const& v, res) {
			topo.is_reconnect(i, v.second, lambda, hairpin_angle);
		};
	}
}

void util::generate_edge_pointers(PREV_NEXT_SEQ & edge_indices, const std::vector<int> & fil_lengths) {
	edge_indices.clear();
	size_t ii = 0;
	size_t is;
	for (std::vector<int>::const_iterator it = fil_lengths.begin(); it != fil_lengths.end(); ++it) {
		is = ii;
		for (size_t i=0; i<*it; ++i) {
			PREV_NEXT_IDX neighbors(ii-1, ii+1);
			edge_indices.push_back(PREV_NEXT(ii, neighbors));
			++ii;
		}
		edge_indices[edge_indices.size()-1].second.second = is;
		edge_indices[is].second.first = edge_indices.size()-1;
	}
}



#define RECON_TIMING 0

void reconnection::reconnect(std::vector<VEC_SEQ > & filaments, double a, double search_size, double lambda, double hairpin_angle, int min_edges) {

	ReconnectionTree tree(filaments, a, search_size, lambda, hairpin_angle);

#if	RECON_TIMING
	tic();
#endif
	tree.edges_to_tree();
#if	RECON_TIMING
	toc_msg() << "edges_to_tree" << std::endl;
	tic();
#endif
	tree.reconnect_tree();
#if	RECON_TIMING
	toc_msg() << "reconnect_tree" << std::endl;
	tic();
#endif
	tree.get_reconnected_filaments(filaments, min_edges);
#if	RECON_TIMING
	toc_msg()<<"get_reconnected_filaments"<<std::endl;
#endif
}

