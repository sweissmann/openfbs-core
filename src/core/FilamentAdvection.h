/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#ifndef FILAMENTADVECTION_H_
#define FILAMENTADVECTION_H_

#include "Filaments.h"
#include "openfbs.h"

namespace openfbs {

typedef std::vector<double> filament_state_type;

class FilamentODE {
public:

	// Singleton stuff:
	static void prepareInstance(
			const std::vector<VEC_SEQ > & _fils,
			double _strength,
			double _a);
	static FilamentODE& getInstance();

	// method to pass to ODE solver:
	static void eval(const filament_state_type & X, filament_state_type & DX_DT, double t);

private:
	FilamentODE();
	FilamentODE(const FilamentODE & copy);
	FilamentODE& operator=(const FilamentODE& copy);
	int n_pts;
	std::vector<int> fil_lengths;
	double strength, a; // filament parameters
	double tol; // tolerance for treecode eval
	int n_leaf; // max leaf size for treecode

	// instance method called through static eval method on singleton instance:
	void evaluate(const filament_state_type & X, filament_state_type & DX_DT, double t);
};

namespace advect {
	void dopri5(std::vector<VEC_SEQ > & filaments, double strength, double a, double dt, int substeps);
	void euler(std::vector<VEC_SEQ > & filaments, double strength, double a, double dt, int substeps);
	void rk4(std::vector<VEC_SEQ > & filaments, double strength, double a, double dt, int substeps);
}

}


#endif /* FILAMENTADVECTION_H_ */
