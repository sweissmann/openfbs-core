/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#include "BiotSavart.h"

#include "Filaments.h"
#include "Vec3d.h"
#include "treecode.h"

namespace openfbs {

const double MAX_ST = 1.0-1E-9;

static double clamp(double d) {
	return (d<-1.0) ? -1.0 : ( (d>1.0) ? 1.0 : d);
}
static double sign(double d) {
	return (d<0.0) ? -1.0 : ( (d>0.0) ? 1.0 : 0.0);
}

static Vec3d biotSavartEdge(
		const double & S2, /*edgeLengthSquared*/
		const Vec3d & v1,
		const Vec3d & v2,
		double a2,
		bool one_point_quadrature) {

	if (S2==0) {
		return ZERO_VEC;
	}

	if (one_point_quadrature) {
		double to_sqr = a2+0.25*(dot(v1,v1)+dot(v2,v2))+0.5*dot(v1,v2);
		double den = -1.0/(to_sqr*sqrt(to_sqr));
		return den*cross(v2, v1);
	}

	double gammaN2 = normSquared(v1);
	double gammaPN2 = normSquared(v2);
	double scpG = dot(v1, v2);
	double f1 = (scpG - gammaN2);
	double g1 = 1.0/sqrt(a2 + gammaN2);
	double f2 = (scpG - gammaPN2);
	double g2 = 1.0/sqrt(a2 + gammaPN2);
	Vec3d ret = cross(v1, v2);
	double d = S2 * a2 + normSquared(ret);
	double f = -(f1*g1 + f2*g2)/d;
	return f*ret;
}


void biotSavartTree(
		VEC_SEQ & ret,
		const VEC_SEQ & eval_pts,
		const std::vector<VEC_SEQ > & filaments,
		double strength,
		double a,
		double eps,
		int max_leaf_size
		) {
	std::vector<int> fil_lens;
	for (size_t i=0; i<filaments.size(); ++i) {
		fil_lens.push_back(filaments[i].size());
	}

	size_t N_PTS=eval_pts.size();
	ret.resize(N_PTS);

	// computes negative of our direct evaluation:
	treecode::compute_velocity(
			&ret[0][0], &eval_pts[0][0], N_PTS,
			&filaments[0][0][0], &fil_lens[0], fil_lens.size(),
			-strength, a, eps, max_leaf_size);
}

void biotSavart(
		VEC_SEQ & ret,
		const VEC_SEQ & eval_pts,
		const std::vector<VEC_SEQ > & filaments,
		double strength,
		double a,
		bool one_point_quadrature)
{
	std::vector<int> filament_lengths;
	VEC_SEQ pts;
	for (std::vector<VEC_SEQ >::const_iterator it=filaments.begin(); it!=filaments.end(); ++it) {
		filament_lengths.push_back(it->size());
		for (int i=0; i<it->size(); i++) {
			pts.push_back((*it)[i]);
		}
	}
	EDGE_SEQ edges;
	util::generate_edge_indices(edges, filament_lengths);
	biotSavartOpt(ret, eval_pts, pts, edges, strength, a, one_point_quadrature);
}


void biotSavartOpt(
		VEC_SEQ & ret,
		const VEC_SEQ & eval_pts,
		const VEC_SEQ & filament_pts,
		const EDGE_SEQ & idx,
		double strength,
		double a,
		bool one_point_quadrature) {

	const double a2 = a*a;
	size_t n_eval_pts = eval_pts.size();
	ret.resize(n_eval_pts);

	size_t n_edges = idx.size();
	for (size_t i_eval = 0; i_eval < n_eval_pts; ++i_eval) {
		Vec3d & vi = ret[i_eval];
		vi = ZERO_VEC;
		const Vec3d & pt = eval_pts[i_eval];
		for (size_t i=0; i<n_edges; ++i) {
			const Vec3d p1 = filament_pts[idx[i].first] - pt;
			const Vec3d p2 = filament_pts[idx[i].second] - pt;
			const double S2 = normSquared(p2-p1);
			vi += biotSavartEdge(S2, p1, p2, a2, one_point_quadrature);
		}
		vi *= 0.25*M_1_PI*strength;
	}
}

void evaluateIntegral(double & result, const double & K, const double & C, const double & D, const double & l) {
	double KK = K*K;
	//			return ExtrapIntegrator.integrate(new RealFunctionOfOneVariable() {
	//				@Override
	//				public double eval(double x) {
	//					double xC=(double) (x+C);
	//					double px = xC*xC+D*D;
	//					return 1.0/(sqrt(KK+px)+K);
	//				}
	//			}, 0, l);
	//		}

	double L1 = C+l;
	double L0 = C;
	double P1 = L1*L1+D*D;
	double P0 = L0*L0+D*D;

	double SR0 = sqrt(KK+P0);
	double SR1 = sqrt(KK+P1);


	double log1 = log(L1+SR1);
	double log0 = log(L0+SR0);

	double lg = log1-log0;

	double atan10, atan11, atan00, atan01, atn;

	if (D>1E-9) {
		atan10 = atan((K*L1)/(D*SR1));
		atan11 = atan(L1/D);

		atan00 = atan((K*L0)/(D*SR0));
		atan01 = atan(L0/D);

		atn = K*(atan10-atan11-atan00+atan01)/D;

		result = lg+atn;
	} else {
		double f1 = -L1/(K+SR1);
		double f0 = -L0/(K+SR0);

		double f = f1-f0;
		result = lg+f;
	}
}

double velocity_flux(const Vec3d & eS, const Vec3d & eT, const Vec3d & eP, const double & a) {

	double ret = 0;

	double aa=a*a;

	Vec3d S(eS), T(eT); // normalized vectors

	Vec3d p(eP); // distance vector between start points

	double L; // length of S-edge
	double l; // length of T-edge
	double pp; // <p,p>

	Vec3d SplusT; // S+T
	Vec3d SminusT; // S-T

	double ST; // <S,T>
	double nSTp; // |S+T| = sqrt(2+2ST)
	double nSTm; // |S-T| = sqrt(2-2ST)

	Vec3d STp; // (S+T)/|S+T|
	Vec3d STm; // (S-T)/|S-T|

	double fSTp; // (S+T)/|S+T|^2
	double fSTm;  // (S-T)/|S-T|^2

	double pSTp; // <p, STp>
	double pSTm; // <p, STm>

	double K;

	Vec3d H; // -(pSTp, pSTm)/SQR2

	Vec3d G1, G2;

	Vec3d A1, A2, A3, A4;

	double C1, C2, C3, C4, DD1, DD2, DD3, DD4, D1, D2, D3, D4;

	double I1, I2, I3, I4; // integrals of parallelogram edges
	double I13, I24, I23;

	L = norm(S);
	l = norm(T);

	if (l==0 || L == 0) return ret;

	normalize(S);
	normalize(T);

	ST = clamp(dot(S,T));

	SplusT = S+T;
	SminusT = S-T;

	pp = normSquared(p);


	nSTp = norm(SplusT);
	nSTm = norm(SminusT);

	STp = SplusT/nSTp;
	STm = SminusT/nSTm;

	pSTp = dot(p, STp);
	pSTm = dot(p, STm);

	H = Vec3d(-pSTm, -pSTp, 0);
	fSTp = -pSTp/nSTp;
	fSTm = -pSTm/nSTm;

	//		if (ST>MAX_ST) H[0]=fSTm=0;
	//		if (ST<-MAX_ST) H[1]=fSTp=0;

	double K2 = aa + pp - dot(H,H);
	if (K2 < 0) K2 = 0;

	K = sqrt(K2);

	G1 = Vec3d(nSTm/2, nSTp/2, 0);
	G2 = Vec3d(nSTm/2, -nSTp/2, 0);

	A1 = Vec3d(H[0], H[1], 0);
	A2 = Vec3d(H[0]+L*G1[0], H[1]+L*G1[1], 0);
	A3 = Vec3d(H[0]+L*G1[0]+l*G2[0], H[1]+L*G1[1]+l*G2[1], 0);
	A4 = Vec3d(H[0]+l*G2[0], H[1]+l*G2[1], 0);

	// initialize done.

	if (ST>MAX_ST || ST<-MAX_ST) {
		double sgn = sign(ST);

		double pT = dot(p,T);
		double KK = aa + pp-pT*pT;

		double XlL=pT+(l-sgn*L);
		double Xl0=pT+(l);
		double X0L=pT+(-sgn*L);
		double X00=pT;

		//(fll-fl0)-(f0l-f00)=fll-fl0-f0l+f00

		double SRlL=sqrt(KK+XlL*XlL);
		double SR00=sqrt(KK+X00*X00);
		double SRl0=sqrt(KK+Xl0*Xl0);
		double SR0L=sqrt(KK+X0L*X0L);

		double flL = SRlL - XlL*log(XlL+SRlL);
		double f00 = SR00 - X00*log(X00+SR00);
		double fl0 = SRl0 - Xl0*log(Xl0+SRl0);
		double f0L = SR0L - X0L*log(X0L+SR0L);
		double parallel = sgn*(flL-fl0-f0L+f00);
		//System.out.println("parallel = "+parallel);
		ret = 0.25*M_1_PI*ST*parallel;
		return ret;
	}


	C1 = dot(A1, G1);
	DD1 = dot(A1, A1)-C1*C1;
	D1 = DD1<=0?0:sqrt(DD1);

	C2 = dot(A2, G2);
	DD2 = dot(A2, A2)-C2*C2;
	D2 = DD2<=0?0:sqrt(DD2);

	C3 = -dot(A3, G1);
	DD3 = dot(A3, A3)-C3*C3;
	D3 = DD3<=0?0:sqrt(DD3);

	C4 = -dot(A4, G2);
	DD4 = dot(A4, A4)-C4*C4;
	D4 = DD4<=0?0:sqrt(DD4);

	evaluateIntegral(I1, K, C1, D1, L);
	evaluateIntegral(I2, K, C2, D2, l);
	evaluateIntegral(I3, K, C3, D3, L);
	evaluateIntegral(I4, K, C4, D4, l);

	I13=(I1-I3);
	I24=(I2-I4);
	I23=L*I2+l*I3;

	ret = 0.25*ST*M_1_PI*(fSTp*(I24+I13) + fSTm*(I24-I13) + I23);

	return ret;
}

double edge_flux(const Vec3d e1, const Vec3d e2, const Vec3d fe1, const Vec3d fe2, const double a) {
	Vec3d eS = fe2-fe1;
	Vec3d eT = e2-e1;
	Vec3d eP = e1-fe1;
	return velocity_flux(eS, eT, eP, a);
}

double evaluateFluxEdge(
		const Vec3d p1, const Vec3d p2, const Vec3d p3, const Vec3d p4,
		const VEC_SEQ & filament_pts,
		const EDGE_SEQ & idx,
		double a
) {
	size_t n_edges = idx.size();

	double f1=0;
	double f2=0;
	double f3=0;
	double f4=0;

	for (size_t i=0; i<n_edges; ++i) {
		const Vec3d fe1 = filament_pts[idx[i].first];
		const Vec3d fe2 = filament_pts[idx[i].second];
		f1 += edge_flux(p1, p2, fe1, fe2, a);
		f2 += edge_flux(p2, p3, fe1, fe2, a);
		f3 += edge_flux(p3, p4, fe1, fe2, a);
		f4 += edge_flux(p4, p1, fe1, fe2, a);
	}
	return f1+f2+f3+f4;
}

void evaluateFlux(
		std::vector<double> & ret,
		const VEC_SEQ & P1,
		const VEC_SEQ & P2,
		const VEC_SEQ & P3,
		const VEC_SEQ & P4,
		const VEC_SEQ & filament_pts,
		const EDGE_SEQ & idx,
		double strength,
		double a
		) {
	int NN = P1.size();
	ret.resize(NN);

	for (size_t i=0; i<NN; ++i) {
		ret[i] = -strength*evaluateFluxEdge(
					P1[i], P2[i], P3[i], P4[i],
					filament_pts, idx, a
				);
	}
}

void quad_energy(double & ret, const Vec3d & p1, const Vec3d & p2, const Vec3d & p3, const Vec3d & p4, const double & a) {

	Vec3d S1 = p2-p1;
	Vec3d S2 = p3-p2;
	Vec3d S3 = p4-p3;
	Vec3d S4 = p1-p4;

	Vec3d   P11 = p1-p1;
	Vec3d   P12 = p2-p1;
	Vec3d   P13 = p3-p1;
	Vec3d   P14 = p4-p1;

	Vec3d   P21 = p1-p2;
	Vec3d   P22 = p2-p2;
	Vec3d   P23 = p3-p2;
	Vec3d   P24 = p4-p2;

	Vec3d   P31 = p1-p3;
	Vec3d   P32 = p2-p3;
	Vec3d   P33 = p3-p3;
	Vec3d   P34 = p4-p3;

	Vec3d   P41 = p1-p4;
	Vec3d   P42 = p2-p4;
	Vec3d   P43 = p3-p4;
	Vec3d   P44 = p4-p4;

	double f11, f12, f13, f14;
	double f21, f22, f23, f24;
	double f31, f32, f33, f34;
	double f41, f42, f43, f44;

	f11 = velocity_flux(S1, S1, P11, a);
	f12 = velocity_flux(S1, S2, P12, a);
	f13 = velocity_flux(S1, S3, P13, a);
	f14 = velocity_flux(S1, S4, P14, a);

	f21 = velocity_flux(S2, S1, P21, a);
	f22 = velocity_flux(S2, S2, P22, a);
	f23 = velocity_flux(S2, S3, P23, a);
	f24 = velocity_flux(S2, S4, P24, a);

	f31 = velocity_flux(S3, S1, P31, a);
	f32 = velocity_flux(S3, S2, P32, a);
	f33 = velocity_flux(S3, S3, P33, a);
	f34 = velocity_flux(S3, S4, P34, a);

	f41 = velocity_flux(S4, S1, P41, a);
	f42 = velocity_flux(S4, S2, P42, a);
	f43 = velocity_flux(S4, S3, P43, a);
	f44 = velocity_flux(S4, S4, P44, a);

	double f1 = f11+f12+f13+f14;
	double f2 = f21+f22+f23+f24;
	double f3 = f31+f32+f33+f34;
	double f4 = f41+f42+f43+f44;

	ret = 0.5*sqrt(f1+f2+f3+f4); // kinetic energy
}

void delta_length(double & ret, const Vec3d & p1, const Vec3d & p2, const Vec3d & p3, const Vec3d & p4) {
	double l1 = norm(p2-p1)+norm(p3-p2)+norm(p4-p3)+norm(p1-p4);
	double l2 = norm(p2-p1)+norm(p4-p2)+norm(p3-p4)+norm(p1-p3);
	ret = l2-l1;
}

double filament_energy(const std::vector< VEC_SEQ > & filaments, double a) {
	double ret = 0;
	for (std::vector< VEC_SEQ >::const_iterator oit = filaments.begin(); oit != filaments.end(); ++oit) {
		const VEC_SEQ & boundary = (*oit);
		const int nb = boundary.size();
		for (std::vector< VEC_SEQ >::const_iterator iit = filaments.begin(); iit != filaments.end(); ++iit) {
			const VEC_SEQ & filament = (*iit);
			const int nf = filament.size();
			for (int i=0; i<nb; ++i) {
				Vec3d p1 = boundary[i];
				Vec3d p2 = boundary[(i+1)%nb];
				for (int j=0; j<nf; ++j) {
					Vec3d f1 = filament[j];
					Vec3d f2 = filament[(j+1)%nf];
					ret += edge_flux(p1, p2, f1, f2, a);
				}
			}
		}
	}
	return ret;
}

}
