/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#include "Subdivider.h"

using namespace openfbs;

static Vec3d twoPointSubdivision(Vec3d z2, Vec3d z3) {
	return 0.5*(z2+z3);
}

static Vec3d fourPointSubdivision(Vec3d z1, Vec3d z2, Vec3d z3, Vec3d z4) {
	static double c1 = -1.0/16.0;
	static double c2 =  9.0/16.0;
	return c1*(z1+z4) + c2*(z2+z3);
}

static Vec3d threePointSubdivision(Vec3d a0, Vec3d a1, Vec3d a2) {
	static double c0 = -1.0/8.0;
	static double c1 = 3.0/4.0;
	static double c2 = 3.0/8.0;
	return c0*a0 + c1*a1 + c2*a2;
}

static void invert(Vec3d & v) {
	double absSquared = normSquared(v);
	v /= absSquared;
}

static bool isTooLong(Vec3d e, Vec3d f, double edgeLengthSquared, double curvatureLengthSquared) {
	double ne=dot(e,e);
	double nf=dot(f,f);
	double s=dot(e,f);
	return ne - s*s/nf > curvatureLengthSquared || ne > edgeLengthSquared;
}

void subdivideLinearOnce(VEC_SEQ & ret, const VEC_SEQ p, bool closed, double minEdgeLengthSquared) {
	int n=p.size();
	ret.clear();
	for (int j=0; j<n; j++) {
		Vec3d q = p[j];
		ret.push_back(q);
		if (!closed && j==n-1) break;
		Vec3d r2 = p[j];
		Vec3d r3 = p[(j+1)%n];
		double edgeLengthSquared = normSquared(r3-r2);
		if (edgeLengthSquared > minEdgeLengthSquared) {
			ret.push_back(0.5*(r2+r3));
		}
	}
}

void subdivideOnce(VEC_SEQ & ret, const VEC_SEQ & p, const bool & closed, const double & edgeLengthSquared, const double & curvatureLengthSquared) {
	int n=p.size();
	ret.clear();

	Vec3d edge1;
	Vec3d edge2;
	Vec3d edge3;
	Vec3d r1;
	Vec3d r2;
	Vec3d r3;
	Vec3d r4;

	int fourPointStart = closed ? 0 : 1;
	int fourPointEnd = closed ? n : n-2;
	if (!closed) {
		// deal with first edge
		Vec3d q = p[0];
		edge1 = p[2] - p[1];
		edge2 = p[1] - p[0];
		if (isTooLong(edge2,edge1,curvatureLengthSquared,edgeLengthSquared)) {
			ret.push_back(threePointSubdivision(p[2],p[1],p[0]));
		}
	}
	for (int j=fourPointStart; j<fourPointEnd; j++) {
		// add the old point
		Vec3d q=p[j];
		ret.push_back(q);
		r1 = p[j == 0 ? n-1 : j-1];
		r2 = p[j];
		r3 = p[(j+1)%n];
		r4 = p[(j+2)%n];
		edge1 = r2-r1;
		edge2 = r3-r2;
		edge3 = r4-r3;
		if (isTooLong(edge2,edge1,curvatureLengthSquared,edgeLengthSquared)
		|| isTooLong(edge2,edge3,curvatureLengthSquared,edgeLengthSquared)) {
			ret.push_back(fourPointSubdivision(r1,r2,r3,r4));
		}
	}
	if (!closed) {
		// deal with last edge
		Vec3d q = p[n-2];
		ret.push_back(q);
		edge1 = p[n-3] - p[n-2];
		edge2 = p[n-2] - p[n-1];
		if (isTooLong(edge2,edge1,curvatureLengthSquared,edgeLengthSquared)) {
			ret.push_back(threePointSubdivision(p[n-3],p[n-2],p[n-1]));
		}
		q = p[n-1];
		ret.push_back(q);
	}
}

void openfbs::subdivide::single(
		VEC_SEQ & ret,
		const VEC_SEQ & p,
		SUBDIVISION_METHOD subdivisionMethod,
		bool closed,
		double maxEdgeLength,
		double maxCurvatureLength) {
	double edgeLengthSquared = maxEdgeLength*maxEdgeLength;
	double curvatureLengthSquared = maxCurvatureLength*maxCurvatureLength;

	static int maxDepth = 10;

	if (&ret == &p) {
		std::cout << "WARNING: inline subdivision not supported!" << std::endl;
		return;
	}
	ret.assign(p.begin(), p.end());
	for (int cnt=0; cnt<maxDepth; ++cnt) {
		VEC_SEQ dst;
		if (subdivisionMethod == CUBIC) {
			subdivideOnce(dst, ret, closed, edgeLengthSquared, curvatureLengthSquared);
		} else if (subdivisionMethod == LINEAR){
			subdivideLinearOnce(dst, ret, closed, edgeLengthSquared);
		}
		bool done = dst.size() == ret.size();
		ret.assign(dst.begin(), dst.end());
		if (done) break;
	}
}

void openfbs::subdivide::all(
		std::vector<VEC_SEQ > & ret,
		const std::vector<VEC_SEQ > & p,
		SUBDIVISION_METHOD subdivisionMethod,
		bool closed,
		double maxEdgeLength,
		double maxCurvatureLength) {
	if (&ret == &p) {
		std::cout << "WARNING: inline subdivision not supported!" << std::endl;
		return;
	}
	ret.resize(p.size());
	std::vector<VEC_SEQ >::iterator dst = ret.begin();
	for (std::vector<VEC_SEQ >::const_iterator ci = p.begin(); ci != p.end(); ++ci) {
		VEC_SEQ subdiv(*ci);
		openfbs::subdivide::single(*dst, *ci, subdivisionMethod, closed, maxEdgeLength, maxCurvatureLength);
		++dst;
	}
}
