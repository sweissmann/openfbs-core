/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#include <math.h>
#include "Vec3d.h"

namespace openfbs
{
   Vec3d::Vec3d() : x(0.), y(0.), z(0.) {}
   Vec3d::Vec3d(double x0, double y0, double z0) : x(x0), y(y0), z(z0) {}
   Vec3d::Vec3d(const Vec3d& v) : x(v.x), y(v.y), z(v.z) {}

   double& Vec3d::operator[](const int& index) { return (&x)[ index ]; }
   const double& Vec3d::operator[](const int& index) const { return (&x)[ index ]; }

   Vec3d Vec3d::operator+(const Vec3d& v) const { return Vec3d(x+v.x, y+v.y, z+v.z); }
   Vec3d Vec3d::operator-(const Vec3d& v) const { return Vec3d(x-v.x, y-v.y, z-v.z); }
   Vec3d Vec3d::operator-() const { return Vec3d(-x, -y, -z); }
   Vec3d Vec3d::operator*(const double& c) const { return Vec3d(x*c, y*c, z*c); }
   Vec3d operator*(const double& c, const Vec3d& v) { return v*c; }
   Vec3d Vec3d::operator/(const double& c) const { return (*this) * (1./c); }
   void Vec3d::operator+=(const Vec3d& v) { x += v.x; y += v.y; z += v.z; }
   void Vec3d::operator-=(const Vec3d& v) { x -= v.x; y -= v.y; z -= v.z; }
   void Vec3d::operator*=(const double& c) { x *= c; y *= c; z *= c; }
   void Vec3d::operator/=(const double& c) { (*this) *= (1./c); }

   Vec3d cross(const Vec3d& u, const Vec3d& v) { return Vec3d(u[1]*v[2]-u[2]*v[1], u[2]*v[0]-u[0]*v[2],u[0]*v[1]-u[1]*v[0]); }
   double dot(const Vec3d& u, const Vec3d& v) { return u[0]*v[0]+u[1]*v[1]+u[2]*v[2]; }
   double normSquared(const Vec3d& u) { return dot(u,u); }
   double norm(const Vec3d& u) { return sqrt(normSquared(u)); }
   void normalize(Vec3d& u) { u /= norm(u); }

   std::ostream& operator << (std::ostream& os, const Vec3d& o) {
	   os << "Vec3d("<<o[0]<<","<<o[1]<<","<<o[2]<<")";
	   return os;
   }
}
