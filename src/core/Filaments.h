/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#ifndef FILAMENTS_H_
#define FILAMENTS_H_

#include "openfbs.h"

#include "Vec3d.h"

namespace openfbs {

namespace util {

	/**
	 * Generates edge indices [p1_idx, p2_idx].
	 */
	void generate_edge_indices( EDGE_SEQ & edge_indices, const std::vector<int> & fil_lengths );

	void read_point_positions( VEC_SEQ & pts, const std::vector<VEC_SEQ > & filaments );
	void write_point_positions( const VEC_SEQ & pts, std::vector<VEC_SEQ > & filaments );

	void generate_filament_lengths( std::vector<int> & fil_lengths, const std::vector<VEC_SEQ > & filaments );

	void ellipse(VEC_SEQ & ret, const size_t npts, const double a, const double b);

}

} /* namespace openfbs */
#endif /* FILAMENTSYSTEM_H_ */
