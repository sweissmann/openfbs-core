/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#ifndef BIOTSAVART_H_
#define BIOTSAVART_H_


#include "openfbs.h"

#include <vector>

namespace openfbs {

void biotSavartOpt(
		VEC_SEQ & ret,
		const VEC_SEQ & eval_pts,
		const VEC_SEQ & filament_pts,
		const EDGE_SEQ & idx,
		double strength,
		double a,
		bool one_point_quadrature=false);

void biotSavart(
		VEC_SEQ & ret,
		const VEC_SEQ & eval_pts,
		const std::vector<VEC_SEQ > & filaments,
		double strength,
		double a,
		bool one_point_quadrature=false
		);

void biotSavartTree(
		VEC_SEQ & ret,
		const VEC_SEQ & eval_pts,
		const std::vector<VEC_SEQ > & filaments,
		double strength,
		double a,
		double eps,
		int max_leaf_size=256
		);

/**
 * Input: two edge vectors, S, T, and a vector P that from the startpoints of S and T.
 */
double velocity_flux(const Vec3d & eS, const Vec3d & eT, const Vec3d & eP, const double & a);

void evaluateFlux(
		std::vector<double> & ret,
		const VEC_SEQ & P1,
		const VEC_SEQ & P2,
		const VEC_SEQ & P3,
		const VEC_SEQ & P4,
		const VEC_SEQ & filament_pts,
		const EDGE_SEQ & idx,
		double strength,
		double a
		);

double filament_energy(const std::vector< VEC_SEQ > & filaments, double a);

void quad_energy(double & ret, const Vec3d & e11, const Vec3d & e12, const Vec3d & e21, const Vec3d & e22, const double & a);

void delta_length(double & ret, const Vec3d & e11, const Vec3d & e12, const Vec3d & e21, const Vec3d & e22);

}

#endif /* BIOTSAVART_H_ */
