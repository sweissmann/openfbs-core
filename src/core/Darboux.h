/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#ifndef DARBOUX_H_
#define DARBOUX_H_

#include "openfbs.h"

namespace openfbs {
namespace darboux {
void single_transform(VEC_SEQ & eta, const VEC_SEQ & gamma, double rodlen /* l */, double twist /* r */);
void double_transform(VEC_SEQ & points, double rodlen /* l */, double twist /* r */);
void step_parameters(double & rodlen, double & twist, double arcLength, int N, double dt, double strength, double a, double mix_parameter);
void time_evolution(std::vector<VEC_SEQ > & fils, double dt, double strength, double a, double mix_parameter);
}
}

#endif /* DARBOUX_H_ */
