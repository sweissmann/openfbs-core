/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#include <vector>
#include "Filaments.h"

using namespace openfbs;

void openfbs::util::generate_filament_lengths(std::vector<int> & fil_lengths, const std::vector<VEC_SEQ > & filaments) {
	fil_lengths.resize(filaments.size());
	for (int i=0; i<filaments.size(); i++) {
		fil_lengths[i] = filaments[i].size();
	}
}

void openfbs::util::read_point_positions( VEC_SEQ & pts, const std::vector<VEC_SEQ > & filaments ) {
	pts.clear();
	for (std::vector<VEC_SEQ >::const_iterator it = filaments.begin(); it != filaments.end(); ++it) {
		for (size_t i=0; i<it->size(); ++i) {
			pts.push_back((*it)[i]);
		}
	}
}

void openfbs::util::write_point_positions( const VEC_SEQ & pts, std::vector<VEC_SEQ > & filaments ) {
	size_t ii=0;
	for (std::vector<VEC_SEQ >::iterator it = filaments.begin(); it != filaments.end(); ++it) {
		for (size_t i=0; i<it->size(); ++i) {
			(*it)[i] = pts[ii];
			++ii;
		}
	}
}

void openfbs::util::generate_edge_indices( openfbs::EDGE_SEQ & edge_indices, const std::vector<int> & fil_lengths ) {
	edge_indices.clear();
	size_t ii = 0;
	size_t is;
	for (std::vector<int>::const_iterator it = fil_lengths.begin(); it != fil_lengths.end(); ++it) {
		is = ii;
		for (size_t i=0; i<*it; ++i) {
			EDGE edge(ii, ii+1);
			++ii;
			edge_indices.push_back(edge);
		}
		edge_indices[edge_indices.size()-1].second = is;
	}
}

void openfbs::util::ellipse(VEC_SEQ & ret, const size_t npts, const double a, const double b) {
	ret.resize(npts);
	double da = 2*M_PI/npts;
	for (size_t i=0; i<npts; i++) {
		double angle = i*da;
		ret[i]=Vec3d(-a*cos(angle), 0, b*sin(angle));
	}
}

