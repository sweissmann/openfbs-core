/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#ifndef VEC3D_H_
#define VEC3D_H_

#include <iostream>

namespace openfbs
{
class Vec3d {
  public:
	 Vec3d();
	 Vec3d(double x, double y, double z);
	 Vec3d(const Vec3d& v);

	 double& operator[] (const int& index);
	 const double& operator[] (const int& index) const;
	 Vec3d operator+(const Vec3d& v) const;
	 Vec3d operator-(const Vec3d& v) const;
	 Vec3d operator-(void) const;
	 Vec3d operator*(const double& c) const;
	 Vec3d operator/(const double& c) const;
	 void operator+=(const Vec3d& v);
	 void operator-=(const Vec3d& v);
	 void operator*=(const double& c);
	 void operator/=(const double& c);

  private:
	 double x, y, z;
};

   Vec3d operator* (const double& c, const Vec3d& v);
   double dot(const Vec3d& u, const Vec3d& v);
   double normSquared(const Vec3d& u);
   double norm(const Vec3d& u);
   void normalize(Vec3d& u);
   Vec3d cross(const Vec3d& u, const Vec3d& v);
   std::ostream& operator << (std::ostream& os, const Vec3d& o);
   const Vec3d ZERO_VEC(0,0,0);
}

#endif

