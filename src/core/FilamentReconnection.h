/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#ifndef FILAMENTRECONNECTION_H_
#define FILAMENTRECONNECTION_H_

#include "openfbs.h"

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/index/rtree.hpp>
#include "Filaments.h"

namespace openfbs {

class FilamentTopology {

public:
	FilamentTopology( const std::vector<VEC_SEQ > & fils, double a );

	bool is_reconnect(const size_t i1, const size_t i2, const double lambda, const double hairpin_angle);

	void do_reconnect(const size_t i1, const size_t i2);

	void convert_to_filaments(std::vector<VEC_SEQ > & fils, const size_t min_edges=4);

	inline void edge_center(Vec3d & cpt, const size_t eidx) {
		EDGE & e = idx[eidx];
		cpt = 0.5*(pts[e.first]+pts[e.second]);
	}

	inline int numEdges() const { return idx.size(); }

private:
	std::vector<bool> edge_labels;
	VEC_SEQ pts;
	PREV_NEXT_SEQ idx_ptr;
	EDGE_SEQ idx;
	double a;
};

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

class ReconnectionTree {

public:
	ReconnectionTree(const std::vector<VEC_SEQ > & fils, double a, double search_size, double lambda, double hairpin_angle);
	void edges_to_tree();
	void reconnect_tree();
	inline void get_reconnected_filaments(std::vector<VEC_SEQ > & fils, const size_t min_edges=4) {
		topo.convert_to_filaments(fils, min_edges);
	}

private:

    typedef bg::model::point<double, 3, bg::cs::cartesian> point;
    typedef std::pair<point, size_t> value;
    typedef bg::model::box< point > box;

    // create the rtree using default constructor
    bgi::rtree< value, bgi::quadratic<16> > rtree;

	const double search_size;
	const double lambda;
	const double hairpin_angle;
	FilamentTopology topo;
};

namespace util {

/**
 * Generates edge pointers of the form [next_edge_idx, point_idx].
 * Used to do reconnection.
 */
void generate_edge_pointers(PREV_NEXT_SEQ & edge_indices, const std::vector<int> & fil_lengths);

}

namespace reconnection {
void reconnect(std::vector<VEC_SEQ > & filaments, double a, double search_size, double lambda, double hairpin_angle, int min_edges);
}

}
#endif /* FILAMENTRECONNECTION_H_ */
