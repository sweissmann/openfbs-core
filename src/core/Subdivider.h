/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#ifndef SUBDIVIDER_H_
#define SUBDIVIDER_H_

#include "openfbs.h"

#include "Vec3d.h"

namespace openfbs {

namespace subdivide {

	enum SUBDIVISION_METHOD {
		LINEAR, CUBIC
	};

	void single(VEC_SEQ & ret,
			const VEC_SEQ & p,
			SUBDIVISION_METHOD subdivisionMethod,
			bool closed,
			double maxEdgeLength,
			double maxCurvatureLength);

	void all(std::vector<VEC_SEQ > & ret,
			const std::vector<VEC_SEQ > & p,
			SUBDIVISION_METHOD subdivisionMethod,
			bool closed,
			double maxEdgeLength,
			double maxCurvatureLength);

}
}

#endif /* SUBDIVIDER_H_ */
