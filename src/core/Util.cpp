/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#include <stack>
#include <iostream>

#include <sys/time.h>

double wclock() {
   timeval tv;
   static struct timezone tz;
   gettimeofday(&tv, &tz);
   return ((double) tv.tv_sec) + ((double) tv.tv_usec ) / (double) 1E6;
}

std::stack<double> tictoc_stack;

void tic() {
    tictoc_stack.push(wclock());
}

inline double toc_pop() {
	double ret = wclock() - tictoc_stack.top();
	tictoc_stack.pop();
	return ret;
}

void toc(const char* msg) {
	for (size_t i=1; i<tictoc_stack.size(); ++i) std::cout<<"-";
    std::cout << "> Time elapsed: " <<toc_pop() << " ["<<msg<<"]"<<std::endl;
}

std::ostream & toc_msg() {
	for (size_t i=1; i<tictoc_stack.size(); ++i) std::cout<<"-";
    std::cout << "> [dt: " <<toc_pop() << "]: ";
    return std::cout;
}
