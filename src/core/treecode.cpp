/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#include "treecode.h"

extern "C" {
	#include "vort_treecode/comp_f.h"
}

#include <cmath>
#include <iostream>
#include <vector>

int openfbs::treecode::compute_velocity(
		double* vel, const double* eval_pts, int n_eval_pts,
		const double* filaments, const int* fil_lengths,	int n_fils,
		double strength, double a, double accuracy, int leaf_size)
{
	double scale = -strength/(4.0*M_PI);

	std::vector<work> scratch;
	size_t fs = 0;
	work ww;
	long N_tot = 0;
	for (size_t i=0; i<n_fils; ++i) {
		size_t n_pts = fil_lengths[i];
		N_tot += n_pts;
		for (size_t j = 0; j<n_pts; ++j) {
		    size_t j1 = (j == n_pts - 1) ? 0 : j + 1;
			double x1 = filaments[fs+3*j];
			double x2 = filaments[fs+3*j1];
			double y1 = filaments[fs+3*j+1];
			double y2 = filaments[fs+3*j1+1];
			double z1 = filaments[fs+3*j+2];
			double z2 = filaments[fs+3*j1+2];
			ww.pos.x1 = 0.5*(x2+x1);
			ww.pos.x2 = 0.5*(y2+y1);
		    ww.pos.x3 = 0.5*(z2+z1);
		    double dx = x2-x1;
		    double dy = y2-y1;
		    double dz = z2-z1;
		    ww.v_wght.x1 = scale*dx;
		    ww.v_wght.x2 = scale*dy;
		    ww.v_wght.x3 = scale*dz;
		    ww.wght_nrm = scale*sqrt(dx*dx+dy*dy+dz*dz);
		    scratch.push_back(ww);
		}
		fs += 3*n_pts;
	}

	tree_cell *root;
	if (!(root = make_tree(0, &scratch[0], N_tot, leaf_size)))
	      return 1;


	vect pt, vv;
	double del2 = a*a;
	for (int i=0; i<n_eval_pts; ++i) {
		pt.x1 = eval_pts[3*i];
		pt.x2 = eval_pts[3*i+1];
		pt.x3 = eval_pts[3*i+2];
		vv = comp_vel(pt, del2, accuracy, root);
		vel[3*i] = vv.x1;
		vel[3*i+1] = vv.x2;
		vel[3*i+2] = vv.x3;
	}

   free_tree();

   return 0;
}

