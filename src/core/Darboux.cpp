/*******************************************************************************
 *
 *     Copyright (c) 2015 Steffen Weissmann (steffen.weissmann@mailbox.org)
 *
 *     This file is part of OpenFBS.
 *
 *     OpenFBS is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     OpenFBS is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with OpenFBS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/
#include "Darboux.h"
#include "Vec3d.h"
#include "boost/math/quaternion.hpp"

#define MAX_ITERS 1000
#define EPS 1E-12

using namespace openfbs;

typedef boost::math::quaternion<double> bquat;

static bquat quat(double re, const Vec3d & im) {
	return bquat(re, im[0], im[1], im[2]);
}

static void assignIm(Vec3d & dst, const bquat & q) {
	dst[0] = q.R_component_2();
	dst[1] = q.R_component_3();
	dst[2] = q.R_component_4();
}

static void darboux_step(Vec3d & ret, const Vec3d & S_i, const Vec3d & lT_i, const double & r) {
	bquat rlT_S = quat(-r, lT_i - S_i);
	bquat lT = quat(0, lT_i);
	bquat lTnext = rlT_S*lT/rlT_S;
	assignIm(ret, lTnext);
}

static void monodromy(Vec3d & ret, const VEC_SEQ & gamma, const Vec3d & lT_1, const double r) {
	size_t n = gamma.size();
	ret = Vec3d(lT_1);
	for (size_t i=0; i<n; i++) {
		Vec3d S_i = gamma[(i+1)%n]-gamma[i];
		darboux_step(ret, S_i, ret, r);
	}
}

static bool power_method(Vec3d & ret, const VEC_SEQ & gamma, const double & l, const double & r) {
	ret = Vec3d(l, 0, 0);
	for (size_t i=0; i<MAX_ITERS; i++) {
		Vec3d lastLT(ret);
		monodromy(ret, gamma, lastLT, r);
		double diff = norm(ret - lastLT);
		if (diff < EPS) {
			return true;
		}
	}
	// signal: "did not converge"
	std::cout << "Darboux transform did not converge!!!"<<std::endl;
	return false;
}

void openfbs::darboux::single_transform(VEC_SEQ & eta, const VEC_SEQ & gamma, double l, double r) {
	size_t n = gamma.size();
	eta.resize(n);
	Vec3d lT;
	if (power_method(lT, gamma, l, r)) {
		for (size_t i=0; i<n; i++) {
			eta[i]=gamma[i]+lT;
			Vec3d S_i = gamma[(i+1)%n]-gamma[i];
			darboux_step(lT, S_i, lT, r);
		}
	} else {
		eta.assign(gamma.begin(), gamma.end());
	}
}

void openfbs::darboux::double_transform(VEC_SEQ & points, double l, double r) {
	using openfbs::darboux::single_transform;

	int N = points.size();
	VEC_SEQ eta(N);
	single_transform(eta, points, l, -r);
	VEC_SEQ eta_rev(N);
	eta_rev.assign(eta.rbegin(), eta.rend());
	VEC_SEQ points_rev(N);
	single_transform(points_rev, eta_rev, l, r);
	points.assign(points_rev.rbegin(), points_rev.rend());
}

static double nGonSpeedBS(const double & R, const double & a, const double & n) {

	double r2 = R*R;
	double r4 = r2*r2;
	double a2 = a*a;

	double sin3 = sin(M_PI/n);
	double cos1 = cos(M_PI/n);
	double sin32 = sin3*sin3;

	double ret=0;
	for (int k=2; k<n; k++) {
		double sinK = sin((k*M_PI)/n);
		double sin_K = sin(((-1 + k)*M_PI)/n);
		double sin22 = sin_K*sin_K;
		double sin_2 = sinK*sinK;
		ret -= r2/M_PI * sin3 * sin_K * sinK * (
				(-4 * r2 * sin22 + 4 * r2 * cos1 * sin_K * sinK) / (sqrt(a2 + 4 * r2 * sin22)
						* (4 * a2 * r2 * sin32 + 16 * r4 * sin32 * sin22 * sin_2))
						-
						(-4 * r2 * cos1 * sin_K * sinK + 4 * r2 * sin_2) / (sqrt(a2 + 4 * r2 * sin_2)
								* (4 * a2 * r2 * sin32 + 16 * r4 * sin32 * sin22 * sin_2)));
	}
	return ret;
}

static double circleSpeed(double R, double a) {
	return 0.25*M_1_PI*(log(8.0*R/a)-1.0)/R;
}

void openfbs::darboux::step_parameters(double & rodlen, double & twist, double arcLength, int N, double dt, double strength, double a, double mix_parameter) {

	double bs_scale = fmin(1.0, 1-mix_parameter);
	double db_scale = fmin(1.0, 1+mix_parameter);

	double L = arcLength;
	double s = L/N;

	//double R = s/(2*Math.sin(Math.PI/n));
	double R = L/(2.*M_PI);

	// calculate the amount of total speed already
	// matched by biot-savart:
	double U = circleSpeed(R, a);
	double vbs = bs_scale*nGonSpeedBS(R, a, N);

	double vsrf = U-vbs;

	// adjust parameters such that the speed is correct
	// for a regular n-gon with edge length sum = poly.getLength()
	// would be correct:

	double stepLen = db_scale*fabs(strength*dt)*vsrf/2.;

	rodlen = sqrt(stepLen*stepLen + s*s);
	twist = stepLen/tan(M_PI/N);
}

static double arc_length( const VEC_SEQ & fil ) {
	double ret = 0.0;
	for (int i=0; i<fil.size(); i++) {
		int j = (i==0) ? fil.size()-1 : i-1;
		ret += norm(fil[i]-fil[j]);
	}
	return ret;
}

void openfbs::darboux::time_evolution(std::vector<VEC_SEQ > & fils, double dt, double strength, double a, double mix_parameter) {
	for (std::vector<VEC_SEQ >::iterator ci = fils.begin(); ci!=fils.end(); ++ci) {
		double L = arc_length(*ci);
		double rodLen, twist;
		openfbs::darboux::step_parameters(rodLen, twist, L, ci->size(), dt, strength, a, mix_parameter);
		openfbs::darboux::double_transform(*ci, rodLen, twist);
	}
}
